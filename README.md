# myvue

#### 一：简单介绍

cmd 创建第一个vue，命名为“myvue”，用IDEA打开文件

#### 二：安装cnpm过程

1. cmd打开命令提示符

2. 安装Node.js淘宝镜像加速器（cnpm）

```
npm install cnpm -g
```
"-g"全局安装

3. 安装“vue-cli”

```
 cnpm install vue-cli -g
```
4. 测试是否安装成功
```
 vue list

```
![查看可以基于哪些模板创建vue应用](https://images.gitee.com/uploads/images/2022/0520/154336_81f4afbb_10630828.png "屏幕截图.png")


#### 三：创建第一个vue-lic应用程序

1.  随便创建一个文件命名为“myvue”

![创建文件myvue](https://images.gitee.com/uploads/images/2022/0520/154833_a764e288_10630828.png "屏幕截图.png")

2. dos命令进入到“myvue”的盘

![](https://images.gitee.com/uploads/images/2022/0520/155039_7f792b80_10630828.png "屏幕截图.png")

3. 创建一个基于webpack模板的vue应用程序，一路enter

初始化文件

```
vue init webpack myvue
```
![输入图片说明](https://images.gitee.com/uploads/images/2022/0520/155409_bf5a4335_10630828.png "屏幕截图.png")

4. 初始化并运行

```
cd myvue
npm install

```

![输入图片说明](https://images.gitee.com/uploads/images/2022/0520/160345_5e03baa2_10630828.png "屏幕截图.png")

5. 运行得到网址

```
npm run dev
```

```
http://localhost:8080
```

![ctrl+c?停止运行](https://images.gitee.com/uploads/images/2022/0520/160554_1404dc33_10630828.png "屏幕截图.png")

6. 访问网址

```
http://localhost:8080
```

7. IDEA打开文件“myvue”

也可运行

```
npm run dev
```


![输入图片说明](https://images.gitee.com/uploads/images/2022/0520/161047_494f3718_10630828.png "屏幕截图.png")

#### 四：补充说明

src下面的可以改变，根据自己的需求

![输入图片说明](https://images.gitee.com/uploads/images/2022/0520/161312_1cd30a14_10630828.png "屏幕截图.png")
