# Spring Boot

#### 一.简单介绍

这个讲的是如何在Spring Boot官网快速建一个Spring Boot项目呢？？跟着步骤走!!

#### 二.new过程


1.  打开Spring官网，找到projects,下拉列表中找到Spring Boot

![输入图片说明](https://images.gitee.com/uploads/images/2022/0524/163841_d4600464_10630828.png "屏幕截图.png")
2.  进入Spring Boot，下拉到底部有一个“Quickstart Your Project”，点击"Spring Initializr"

![输入图片说明](https://images.gitee.com/uploads/images/2022/0524/164101_a277664b_10630828.png "屏幕截图.png")
3.  进入“Spring Initializr”,按照指示进行选择

![输入图片说明](https://images.gitee.com/uploads/images/2022/0524/164633_1cb9bd73_10630828.png "屏幕截图.png")
4. 点击添加依赖，搜索“Web”就行了

![输入图片说明](https://images.gitee.com/uploads/images/2022/0524/164727_6600b664_10630828.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0524/164845_4f957491_10630828.png "屏幕截图.png")
5. 保存到本地（本机保存到了D盘的Web Download文件）

![输入图片说明](https://images.gitee.com/uploads/images/2022/0524/165007_c87214ea_10630828.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0524/165044_17d9aa42_10630828.png "屏幕截图.png")

#### 三.启动IDEA

1. file--new--Projects From Existing Sources

![输入图片说明](https://images.gitee.com/uploads/images/2022/0524/170531_f2241388_10630828.png "屏幕截图.png")
2. 开始import

![输入图片说明](https://images.gitee.com/uploads/images/2022/0524/170650_99b6ad67_10630828.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0524/170759_4d6cfb67_10630828.png "屏幕截图.png")
3. 选maven 之后一路next
4. 耐心等待，乐色电脑有点慢

![输入图片说明](https://images.gitee.com/uploads/images/2022/0524/171033_3e1a7fff_10630828.png "屏幕截图.png")
5. 终于好了

![输入图片说明](https://images.gitee.com/uploads/images/2022/0524/171158_638adf57_10630828.png "屏幕截图.png")

#### 四.是不是感觉有点麻烦

1. 那就直接启动IDEA

![输入图片说明](https://images.gitee.com/uploads/images/2022/0524/171542_c355eed7_10630828.png "屏幕截图.png")
2. next到选择依赖的时候照样选择 Web

![输入图片说明](image.png)

