import Vue from 'vue'
import Router from 'vue-router'
import Content from '../components/Content'
import Main from '../components/Main'
import ZhangSan from '../components/ZhangSan'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/content',
      name: 'content',
      component: Content
    },
    {
      path: '/main',
      name: 'main',
      component: Main
    },
    {
      path: '/zhangsan',
      name: 'zhangsan',
      component: ZhangSan
    }
  ]
})
