# about vue router

#### 介绍

之前接着myvue里面的，vue router，实际上我们要操作的是在

1. 目录components下新建自己需要的vue文件

![输入图片说明](https://images.gitee.com/uploads/images/2022/0523/150327_f9869d31_10630828.png "屏幕截图.png")

2. 在router目录下面的index.js文件配置路由

![输入图片说明](https://images.gitee.com/uploads/images/2022/0523/150444_59036a9a_10630828.png "屏幕截图.png")

3. 在App.vue里面引用，然后直接运行

![输入图片说明](https://images.gitee.com/uploads/images/2022/0523/150558_ba8cad22_10630828.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0523/150512_f626a65c_10630828.png "屏幕截图.png")

#### 安装vue-router    

1. 先把不相关文件删掉，再在IDEA里面执行这条命令

把IDEA里面新建的myvue文件里面不相关的都删掉（logo.png,helloworld.vue,test.js）,
实际上我们要操作的是main.js,App.vue,注意这些文件里面关于helloworld的设置配置啥的也要删除掉。

 `npm install vue-router --save-dev`

![npm install vue-router --sass-dev](https://images.gitee.com/uploads/images/2022/0523/141322_1e0bbb4f_10630828.png "屏幕截图.png")


2. 安装完vue-router后在"main.js"里面import router

在main里面显示说明使用use router
 
`Vue.use(VueRouter)
`

![import router](https://images.gitee.com/uploads/images/2022/0523/141222_c99d2422_10630828.png "屏幕截图.png")

3. 测试安装

`npm run dev`

![测试结果](https://images.gitee.com/uploads/images/2022/0523/142027_6e127b09_10630828.png "屏幕截图.png")

注意：测试结果成功的是空白页，因为自己的App.vue里面没有东西可以自己加东西（当增加东西的时候它会自动刷新，这个叫做 **热部署** ）

![输入图片说明](https://images.gitee.com/uploads/images/2022/0523/142236_f4359397_10630828.png "屏幕截图.png")


#### 开始建文件写

1. 在components目录下新建一个vue文件--“Content.vue”

![输入图片说明](https://images.gitee.com/uploads/images/2022/0523/143005_3bc01a9c_10630828.png "屏幕截图.png")

2. 配置路由（在router目录下面的index.js里面配置）

--其实我们在建文件myvue的时候选择了自动配置，所有这个router目录跟index.js是自动生成的，不需要我们手动去配。我们需要做的是import 组件

`path:'/content',----路径
name: 'content',----命名
component:Content----配置跳转到主键，可跳转到Content.vue`


![输入图片说明](https://images.gitee.com/uploads/images/2022/0523/143521_c5bd954b_10630828.png "屏幕截图.png")

3. 在components目录下面新建一个vue文件--“Main.vue”

![输入图片说明](https://images.gitee.com/uploads/images/2022/0523/144216_d4e23474_10630828.png "屏幕截图.png")

4. 新建了Main.vue之后想用怎么办，那就去index.js里面去配置（改正一下：图片中的“主键”改为“组件”）

![输入图片说明](https://images.gitee.com/uploads/images/2022/0523/144435_d3d82a7d_10630828.png "屏幕截图.png")

5. 做完这些之后想用怎么办，去找App.vue,直接调用！

![输入图片说明](https://images.gitee.com/uploads/images/2022/0523/145236_664afbcb_10630828.png "屏幕截图.png")

#### 测试结果

![输入图片说明](https://images.gitee.com/uploads/images/2022/0523/145641_79424e6e_10630828.png "屏幕截图.png")